const express = require('express')
const router=express.Router()
const viewsController=require('../controllers/viewController')

router.get('/login',viewsController.getLoginForm)
router.get('/signup',viewsController.getSignupForm)
router.get('/',viewsController.getHome)




module.exports=router